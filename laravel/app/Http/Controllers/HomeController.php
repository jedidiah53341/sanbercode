<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function go_to_home() {
		return view('/home');
	}
}
