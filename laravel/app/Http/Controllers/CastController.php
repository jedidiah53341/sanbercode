<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CastController extends Controller
{

	// Tampilkan data
      public function index()
    {
        $post = \DB::table('cast')->get();
        return view('index', compact('post'));
    }

    // Masuk ke view entri cast baru
    public function create()
    {
        return view('create');
    }

    // Memasukkan entri cast baru
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        $query = \DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');
    }

    // Tampilkan detail cast dengan id tertentu
    public function show($id)
    {
        $post = \DB::table('cast')->where('id', $id)->first();
        return view('show', compact('post'));
    }

    //Masuk ke view edit cast
    public function edit($id)
    {
        $post = \DB::table('cast')->where('id', $id)->first();
        return view('edit', compact('post'));
    }

    // Mengupdate entri cast
    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $query = \DB::table('cast')
            ->where('id', $id)
            ->update([
                "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
            ]);
        return redirect('/cast');
    }

    //Hapus entri cast
    public function destroy($id)
    {
        $query = \DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
