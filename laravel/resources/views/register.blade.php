<!DOCTYPE html>
<html>
<head>
<title>Sign Up!</title>

<style>
.center {
  margin: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align:center;
}
</style>

</head>
<body class='center' >

<h1>Buat Account Baru!</h1>

<form  action="/kirim" method="post" id="signup_form">
@csrf
<h3>Sign Up Form</h3>

<label>First name:</label><br>
<input type="text" name="firstname"><br>

<label>Last name:</label><br>
<input type="text" name="lastname"><br>

<label>Gender :</label><br>
<input type="radio" value="Male" >Male</radio><br>
<input type="radio" value="Female" >Female</radio><br>
<input type="radio" value="Other" >Other</radio><br>

<label>Nationality :</label><br>
<select name="nationality">
<option value="Indonesian">Indonesian</option>
<option value="Malaysian">Malaysian</option>
<option value="Singaporean">Singaporean</option>
<option value="Australian">Australian</option>
</select><br>

<label>Gender :</label><br>
<input type="checkbox" value="Bahasa" >Bahasa Indonesia</radio><br>
<input type="checkbox" value="English" >English</radio><br>
<input type="checkbox" value="Other" >Other</radio><br>

<label>Bio :</label><br>
<textarea></textarea><br>
<button><a  style="text-decoration:none;color:black;">Sign Up</a></button>
</form>


</body>
</html>

