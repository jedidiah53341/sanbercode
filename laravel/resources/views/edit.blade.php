<div>
    <h1>/cast/{cast_id}/edit</h1>
<h6>menampilkan form untuk edit pemain film dengan id tertentu</h6>
<hr>
<h2>Id cast : {{$post->id}}</h2>
        <h2>Edit Cast dengan id : {{$post->id}}</h2>

        <form  action="/cast/{{$post->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="title">Nama</label>
                <input type="text" class="form-control" name="nama" value="{{$post->nama}}" id="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">umur</label>
                <input type="text" class="form-control" name="umur"  value="{{$post->umur}}"  id="umur" placeholder="Masukkan umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">bio</label>
                <input type="text" class="form-control" name="bio"  value="{{$post->bio}}"  id="bio" placeholder="Masukkan bio">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Update!</button>
        </form>
    </div>