<style>

a {
    text-decoration: none;
    padding:2%;
    color:white;
    background:hsl(220,100%,30%);
    margin-bottom:2%;
    font-weight:bold;
    display:block;
}
table {
    border:1px solid black;
}
</style>

<h1>/cast</h1>
<h6>menampilkan list data para pemain film (boleh menggunakan table html atau bootstrap card)</h6>


<a href="/cast/create" >Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">Id</th>
                <th scope="col">Nama Aktor</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col" style="display: inline">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($post as $key=>$value)


                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->umur}}</td>
                        <td>{{$value->bio}}</td>
                        <td>
                            <a href="/cast/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/cast/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>