<?php
include 'animal.php';
include 'Frog.php';
include 'Ape.php';
$sheep = new Animal("shaun");

echo "Name : ";
echo $sheep->get_name(); // "shaun"
echo "<br>legs : ";
echo $sheep->legs; // 4
echo "<br>cold blooded : ";
echo $sheep->cold_blooded; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())


echo "<br><br>";
$kodok = new Frog("buduk");
echo "Name : ";
echo $kodok->get_name();
echo "<br>legs : ";
echo $kodok->legs; // 4
echo "<br>cold blooded : ";
echo $kodok->cold_blooded; // "no"
echo "<br>Jump : ";
$kodok->jump() ; // "hop hop"


echo "<br><br>";
$sungokong = new Ape("kera sakti");
echo "Name : ";
echo $sungokong->get_name();
echo "<br>legs : ";
echo $sungokong->legs; // 4
echo "<br>cold blooded : ";
echo $sungokong->cold_blooded; // "no"
echo "<br>Yell : ";
$sungokong->yell(); // "Auooo"

?>